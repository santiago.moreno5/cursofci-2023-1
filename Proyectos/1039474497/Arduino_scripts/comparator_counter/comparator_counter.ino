/************************DPC_UdeA**************************************
circuit ADS1115 - Arduino:

VDD -> +5 V
GND -> GND
SCL -> SCL
SDA -> SDA
ADDR -> GND (0X48)
ALRT -> LED
A0 -> POT

This script reads the ADC value in A0 (ADS1115 module), converts the value to voltage
and changes the window threshold of the comparator automatically every 5 seconds.
When voltage value reaches the threshold window range, a counter is increased


Authors: Sebastian Montoya, Luis Felipe Ramírez
ADS1X15 Library author: Rob.Tillaart 

*/

#include "ADS1X15.h"

ADS1115 ADS(0x48);

//float f = ADS.toVoltage(1);  // voltage factor
const float Th_step = 0.5; // threshold window hight
int counter = 0;
//int16_t val_0;
//float f;


String inputString;


int start_time;
int end_time;
float threshold = 0.0; // setting threshold
int t_win = 5000; //time window
const float V = 4.5; // max voltage measurement

void setup() {
  
  Serial.begin(115200);
  ADS.begin();
  ADS.setGain(0);
  ADS.setComparatorMode(1);              // 0 = TRADITIONAL    1 = WINDOW
  ADS.setComparatorPolarity(0);          // 0 = LOW (default)  1 = HIGH
  ADS.setDataRate(7); //default 128 sps, 7 -> 860 sps. (8,16,32,64,128,250,475,860)
  
  // note NON-LATCH gives only a short pulse
  ADS.setComparatorLatch(1);             // 0 = NON LATCH      1 = LATCH
  ADS.setComparatorQueConvert(0);        // 0 = trigger alert after 1 conversion
  start_time=millis();
}

void loop() {
  

  
  float f = ADS.toVoltage(1);  // voltage factor
  int16_t val_0 = ADS.readADC(0); 

  end_time=millis()-start_time; //clock setting
  
  if(end_time>t_win){ // when fixed time window is reached
    if(threshold<V){
      threshold += Th_step; // threshold value increases in 0.01 V
    }
    ADS.setComparatorThresholdLow(threshold/f); //setting threshold values
    ADS.setComparatorThresholdHigh((threshold+Th_step)/f);
    start_time=millis();
    counter = 0;
  }

  if (val_0*f < ADS.getComparatorThresholdHigh()*f & val_0*f > ADS.getComparatorThresholdLow()*f){ //this counts if voltage value is in the window
      counter++;
    }
  

  Serial.print("t: ");
  Serial.print('\t');
  Serial.print(millis()/1000);
  Serial.print('\t');
  Serial.print(val_0 * f, 3);
  Serial.print('\t');
  Serial.print(ADS.getComparatorThresholdLow() * f, 5);
  Serial.print('\t');
  Serial.print(ADS.getComparatorThresholdHigh() * f, 5);
  Serial.print('\t');
  Serial.print(counter);
  Serial.println();

 //Serial.println(String(ADS.getComparatorThresholdLow() * f,5) + "%" + String(ADS.getComparatorThresholdHigh() * f,5) + "$" + String(counter));
 
  delay(100);

  SerialEvent();
  }

void SerialEvent(){
    while(Serial.available()){
      char inputChar = Serial.read();
      inputString += inputChar;
      }
      if(inputString.indexOf("getValue")>=0){ 
    } 
    inputString = "";
}
  
  
