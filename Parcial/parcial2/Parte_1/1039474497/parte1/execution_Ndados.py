from Ndados import SistemaDados, Dado
import matplotlib.pyplot as plt

if __name__=="__main__":

    # Número de lanzamientos
    lanzamientos = 1000000

    # Crear sistemas de dados con diferentes valores de N
    sistema2 = SistemaDados(2)
    sistema3 = SistemaDados(3)
    sistema4 = SistemaDados(4)

    # Lanzar dados consecutivamente
    resultados_consecutivos2 = sistema2.tirar_consecutivamente(lanzamientos)
    resultados_consecutivos3 = sistema3.tirar_consecutivamente(lanzamientos)
    resultados_consecutivos4 = sistema4.tirar_consecutivamente(lanzamientos)

    # Lanzar dados simultáneamente
    resultados_simultaneos2 = sistema2.tirar_simultaneamente(lanzamientos)
    resultados_simultaneos3 = sistema3.tirar_simultaneamente(lanzamientos)
    resultados_simultaneos4 = sistema4.tirar_simultaneamente(lanzamientos)

   

    # Graficar los resultados
    plt.figure(figsize=(18, 12))

    # Histograma para lanzamientos consecutivos
    plt.subplot(2, 3, 1)
    plt.hist(resultados_consecutivos2, bins=11, range=(1.5, 12.5), edgecolor='black')
    plt.title('N = 2 (Consecutivo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    plt.subplot(2, 3, 2)
    plt.hist(resultados_consecutivos3, bins=16, range=(2.5, 18.5), edgecolor='black')
    plt.title('N = 3 (Consecutivo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    plt.subplot(2, 3, 3)
    plt.hist(resultados_consecutivos4, bins=21, range=(3.5, 24.5), edgecolor='black')
    plt.title('N = 4 (Consecutivo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    # Histograma para lanzamientos simultáneos
    plt.subplot(2, 3, 4)
    plt.hist(resultados_simultaneos2, bins=11, range=(1.5, 12.5), edgecolor='black')
    plt.title('N = 2 (Simultáneo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    plt.subplot(2, 3, 5)
    plt.hist(resultados_simultaneos3, bins=16, range=(3.5, 19.5), edgecolor='black')
    plt.title('N = 3 (Simultáneo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    plt.subplot(2, 3, 6)
    plt.hist(resultados_simultaneos4, bins=21, range=(4.5, 25.5), edgecolor='black')
    plt.title('N = 4 (Simultáneo)')
    plt.xlabel('Suma')
    plt.ylabel('Número de microestados')

    plt.tight_layout()
    plt.savefig("Ndados_hist.png")
    plt.show()
