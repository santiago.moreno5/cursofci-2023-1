import numpy as np
import matplotlib.pyplot as plt
import sympy

class PenduloSimple():
    '''Clase Péndulo Simple, soluciona numérica y analíticamente la aproximación
        del péndulo simple como oscilador armónico cuando tiene pequeñas oscilaciones
        Inputs:
        - CondInicial = [theta(0),d_Theta(0)]:  desplazamiento angular y Velocidad Angular en el tiempo inicial
        - Omega = Frecuencia angular: raiz cuadrada de la relación entre gravedad y longitud del péndulo  
        - Tiempo final: Tiempo final de la solución
        Opcional:
        - Tiempo inicial'''
    

    # Constructor
    def __init__(self,CondInicial,Omega,TiempoFinal,TiempoInicial = 0):

        # Verificando que las condiciones iniciales permitan una solución
        if (not any(CondInicial)) or (Omega == 0):
            print('Valores ingresados no son válidos')
            raise ValueError
        else:           
            self.Omega = Omega
            self.C_0 = CondInicial
            self.t_f = TiempoFinal
            self.t_0 = TiempoInicial


    # Ecuación del péndulo simple bajo la aproximación de pequeñas oscilaciones
    def EcuacionPendulo(self,S_t):
        d_Theta = S_t[1]
        dd_Theta = -S_t[0]*self.Omega**2
        return np.array([d_Theta,dd_Theta])
    
    # Definimos método de Euler para vectores
    def Euler(self,X,Y,h,i):
        Yi = [Y[i]+h*self.EcuacionPendulo(Y[i])]
        Xi = X[i] + h
        return Xi,Yi
    
    # Definimos método de Runge-Kutta 4 para vectores
    def Rk4(self,X,Y,h,i):
        k1 = self.EcuacionPendulo(Y[i])
        k2 = self.EcuacionPendulo(Y[i]+0.5*k1*h)
        k3 = self.EcuacionPendulo(Y[i]+0.5*k2*h)
        k4 = self.EcuacionPendulo(Y[i]+k3*h)
        Xi = X[i]+h
        Yi = [Y[i]+h*(k1+2*k2+2*k3+k4)/6]
        return Xi,Yi
    
    # Solución Analítica bajo reducción de Euler
    def Sympy_Sol(self,X,Y,h,Nmax):

        # Definiendo variables simbólicas
        t = sympy.Symbol('t')
        Omg = sympy.Symbol('Omega')
        Th = sympy.Function('theta')
        y = sympy.Function('y')


        F = self.EcuacionPendulo([Th(t),y(t)])

        # Por el editor salen "Errores" pero en el testeo no hubo ningún error

        # Condiciones iniciales, partiendo de t = 0
        condInit = {Th(t).subs(t,self.t_0):self.C_0[0],y(t).subs(t,self.t_0):self.C_0[1] }

        # Ecuaciones a resolver
        Ecuaciones = [sympy.Eq(Th(t).diff(t),F[0]),sympy.Eq(y(t).diff(t),F[1])]

        # Solucionando las ecuaciones
        sol_ed = sympy.dsolve(Ecuaciones,[Th(t),y(t)],ics= condInit)

        # Solución desplazamiento Angular
        Theta = sol_ed[0].subs(Omg,4)
        Theta = sympy.simplify(Theta)

        # Solución Velocidad angular
        d_Theta = sol_ed[1].subs(Omg,4)
        d_Theta = sympy.simplify(d_Theta)


        # Rellenando arreglos
        for i in range(0,Nmax):
            Xi = X[i]+h
            X = np.append(X,Xi)
            Yi = [Theta.rhs.subs(t,X[i+1]).evalf(),d_Theta.rhs.subs(t,X[i+1]).evalf()]        
            Y = np.append(Y,[Yi],axis=0)
        return X,Y
    
    # Solución en el intervalo temporal de [t_0,t_f]
    def Solucion_t(self,method='Euler',N = 500):

        X = np.array([0])
        Y = np.array([self.C_0])

        h = (self.t_f-self.t_0)/int(N)

        # Verificando que el método no ingresado está dentro de la clase
        if not method in ['Euler','RK4','Sympy']:
            print('Método no encontrado')
            raise ValueError
    
        def Sol_method(X,Y,h,i): return None,None # Inicializando La variable que contendrá el método a utilizar

        if method == 'Euler':
            Sol_method = self.Euler
            
        if method == 'RK4':
            Sol_method = self.Rk4
        
        if method == 'Sympy':
            X,Y = self.Sympy_Sol(X,Y,h,Nmax=N)
            return X,Y         

        # Rellenando arreglos
        for i in range(0,N):
            Xi,Yi = Sol_method(X,Y,h,i)
            X = np.append(X,Xi)
            Y = np.append(Y,Yi,axis=0)
        return X,Y

    # Funcion que devuelve el desplazamiento angular en todo el intervalo
    def DesplazamientoAngular(self,method='Euler'):
        X, Y = self.Solucion_t(method=method)
        return Y[:,0]
    
    # Funcion que devuelve el desplazamiento angular en todo el intervalo
    def VelocidadAngular(self,method='Euler'):
        X, Y = self.Solucion_t(method=method)
        return Y[:,1]
    
    # Grafica de las soluciones
    def Grafica(self,Coord = 0,method = 'Euler',name='Pendulo_Simple',N = 500):
        # Coord = 0: Desplazamiento Angular
        # Coord = 1: Velocidad Angular

        X,Y = self.Solucion_t(method = method,N=N)

        plt.figure()
        plt.plot(X,Y[:,Coord], label = 'Solución Numérica ({})'.format(method))

        Coord_name = '$\\frac{d}{dt}$ ' if Coord else ''
        plt.xlabel('t')
        plt.ylabel(Coord_name+'$\\theta$(t)')
        plt.grid()
        plt.legend()
        plt.title('Soluciones '+name)
        plt.savefig('Plot_'+name+'.png')
    
    # Comparación entre dos soluciones
    def GraficaComparacion(self,Coord = 0,method = 'Euler',method2 = 'Sympy',name='Pendulo_Simple',N = 500):

        X_exact,Y_exact = self.Solucion_t(method=method2,N=N)
        X_numeric,Y_numeric = self.Solucion_t(method = method,N=N)

        plt.figure()
        plt.plot(X_exact,Y_exact[:,Coord], label = 'Solución Exacta')
        plt.plot(X_numeric,Y_numeric[:,Coord], label = 'Solución Numérica ({})'.format(method))

        Coord_name = '$\\frac{d}{dt}$ ' if Coord else ''
        plt.xlabel('t')
        plt.ylabel(Coord_name+'$\\theta$(t)')
        plt.grid()
        plt.legend()
        plt.title('Comparación Soluciones '+name)
        plt.savefig('Plot_'+name+'.png')