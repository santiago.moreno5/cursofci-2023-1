import math
import numpy as np
import matplotlib.pyplot as plt


class tiroParabolico():
    def __init__(self, velinit, alpha, g, h0, x0,a):
        print("inicializando clase tiroParabolico")

        # Parametros
        self.velinit = velinit
        self.radianalpha = math.radians(alpha)
        self.g = g
        self.h0 = h0
        self.x0 = x0
        self.a = a

    def velX(self):
        vel_x = self.velinit * round(math.cos(self.radianalpha), 3)
        return vel_x

    def velY(self):
        vel_y = self.velinit * round(math.sin(self.radianalpha), 3)
        return vel_y

    def tMaxVuelo(self):
        try:
            tmax = (-(self.velY()) - np.sqrt(self.velY() ** 2 - 2 * self.g * self.h0)) / (self.g)
            return tmax
        except:
            return "error en calculo de tmax, revise parametros"

    def arrTime(self,t):
        arr_time = np.arange(0, t, 0.001)
        return arr_time

    def posX(self):
        posx = [self.x0 + i * self.velX() + 0.5 * self.a * i ** 2 for i in self.arrTime(self.tMaxVuelo())]
        return posx

    def posY(self):
        posy = [self.h0 + i * self.velY() + 0.5 * self.g * i ** 2 for i in self.arrTime(self.tMaxVuelo())]
        return posy

    def alcanceMaximo0(self):
        tm = self.tMaxVuelo()
        vx=self.velX()
        alcance = self.x0 + vx*tm + 0.5 * (self.a) * self.tMaxVuelo()** 2
        return alcance

    def alturaMaxima0(self):
        altura = -self.h0+(self.velinit ** 2) * (math.sin(self.radianalpha) ** 2) / (2 * self.g)
        return altura

    def figParabolico(self):
        plt.figure(figsize=(10, 8))
        plt.plot(self.posX(), self.posY())
        plt.text(self.x0, 0, 'a = {} m/s2\n g = {} m/s2\nV0 = {} m/s \nt vuelo = {:.1f} s \n x max = {:.1f} m \n y max = {:.1f} m'.format(self.a,self.g, self.velinit, self.tMaxVuelo(), self.alcanceMaximo0(), -self.alturaMaxima0()), bbox=dict(facecolor='red', alpha=0.5))
        plt.title("Trayectoria movimiento parabolico simple")
        plt.xlabel("x[m]")
        plt.ylabel("y[m]")
        plt.savefig("parabolico1.png")


class rifle(tiroParabolico):
    """
    Clase que modela el comportamiento de la munición de algunos modelos de rifle según los parametros del fabricante.
    Hereda de la clase tiroParabolico.
    """
    
    def __init__(self, modelo,h0,a,x0=0,velinit=0,alpha=0, g=-9.8, m=0):
        print("Inicializando clase caracterizacion de rifles")
        super().__init__(velinit,alpha, g, h0, x0,a)
        self.modelo = modelo
        self.m = m

        if self.modelo == "AK47":
                self.velinit = 715
                self.m = 0.008
        if self.modelo == "AR15":
            self.velinit = 1006
            self.m = 0.004
        if self.modelo == "M16":
            self.velinit = 948
            self.m = 0.005
        if self.modelo == "A91":
            self.velinit = 270
            self.m = 0.0039

    def momento(self):
        p = self.m * self.velinit
        return p
        
     #Hacemos una funcion que seleccione v0 segun el modelo del rifle   
    def caracteristicas(self):
        return print("La velocidad del rifle {} es {} m/s \nSu alcance maximo es {:.2f} m \nEl momento de su munición (sin rozamiento) es de {} Kgm/s".format(self.modelo, self.velinit, self.alcanceMaximo0(),self.momento()))
    

    def figRifle(self):
        plt.figure(figsize=(10, 8))
        plt.plot(self.posX(), self.posY())
        plt.text(self.x0, 0, 'a = {} m/s2\n g = {} m/s2\nV0 = {} m/s \nt vuelo = {:.1f} s \n x max = {:.1f} m \n y max = {:.1f} m'.format(self.a,self.g, self.velinit, self.tMaxVuelo(), self.alcanceMaximo0(), -self.alturaMaxima0()), bbox=dict(facecolor='red', alpha=0.5))
        plt.title("Trayectoria municion de rifle {}".format(self.modelo))
        plt.xlabel("x[m]")
        plt.ylabel("y[m]")
        plt.savefig("trayectoriaBala.png")
    





   


    