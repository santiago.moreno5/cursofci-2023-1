from PenduloSimple import penduloSimple

if __name__ == "__main__":
    
    # Definimos los parametros dados para instanciar la clase

    tiempo_a = 0
    tiempo_b = 5
    theta_CI = 1
    omega_CI = 0
    w_0 = 4
    h = 0.1

    # Creamos un objeto de la clase penduloSimple con estos parametros

    pendulo_simple = penduloSimple(tiempo_a, tiempo_b, theta_CI, omega_CI, w_0, h)
    
    # Se genera la gráfica de desplazamiento angular que compara a la vez ambas soluciones
    
    pendulo_simple.DesplazamientoAngular()
