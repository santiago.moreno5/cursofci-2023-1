
import numpy as np
import matplotlib.pyplot as plt


class trayectorias:
    
    #Método constructor

    def __init__(self,alpha,x0,v0,h0,g):
        self.alpha=alpha
        self.x0=x0
        self.v0=v0
        self.h0=h0
        self.g=g
 
    #Construcción de los metodos necesarios para describir un movimiento de tipo tiroparabólico  
    
    def v0x(self):
        return self.v0*round(np.cos(self.alpha),3)
    
    def v0y(self):
        return self.v0*round(np.sin(self.alpha),3)    

    def time(self):
        return -self.v0y()/self.g

    def time_v(self):
        t1=np.linspace(0,2*self.time(),500)
        return t1
    
    def pos_x(self):
        return self.x0+self.v0x()*self.time_v()
    
    def pos_y(self):
        return self.h0+self.v0y()*self.time_v()+(1/2)*self.g*(self.time_v())**2
    #Plot
    def plot_t(self):
        plt.figure(figsize=(8,6))
        plt.plot(self.pos_x(),self.pos_y(),color="r")
        plt.grid()
        plt.title("Trayectoria")
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.savefig("tiro_parabolico_y.png")
        
#Movimiento con una fuerza en el eje x
class trayectoria_x(trayectorias):
    def __init__(self, alpha, x0, v0, h0, g,g_x):
        self.g_x=g_x
        super().__init__(alpha, x0, v0, h0, g)

    def posxx(self):
        return self.v0x()*self.time_v()+(1/2)*self.g_x*(self.time_v())**2
    

    def plot_t2(self):
        plt.figure(figsize=(8,6))
        plt.plot(self.posxx(),self.pos_y(),color="g")
        plt.grid()
        plt.title("Trayectoria 2")
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.savefig("tiro_parabolico_xy.png")