#importamos las librerias necesarias
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#se define la clase
class movParticula():
    def __init__(self, tiempo, energia_particula, masa_particula, carga_particula, campo_magnetico, angulo):
        
        self.B = campo_magnetico  # campo magnético
        self.m = masa_particula  # masa 
        self.t = tiempo  # tiempo 
        self.Ek = energia_particula  # energía cinética de la partícula
        self.q = carga_particula  # carga partícula
        self.ang = angulo  # ángulo respecto de v a B
        self.w = abs(self.q) * self.B / self.m  # w
        self.v = np.sqrt(self.Ek * 2 / self.m)  # despejamos v_0 de Ek
        self.v_c = self.v * np.sin(np.pi / 180 * self.ang)  # componente de la velocidad que genera curvatura
        self.r = self.v_c * self.m / (abs(self.q) * self.B)  # radio de la trayectoria
        
    # movimiento en x
    def pos_x(self):
        x = self.r * np.sin(self.w * self.t)  
        return x
    # movimiento en y
    def pos_y(self):
        y = self.r * np.cos(self.w * self.t)  
        return y
    # movimiento en z
    def pos_z(self):
        vz = self.v * np.cos(np.pi / 180 * self.ang)  
        z = vz * self.t  
        return z
    # periodo
    def periodo(self):
        T = 2 * np.pi / self.w   
        return T
    #se grafica
    def plot_traj(self):

        fig = plt.figure()
        
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(self.pos_x(), self.pos_y(), self.pos_z(), "g",
                 label="Ek = {:.2E} J \nB = {} T \n$\phi$ = {}° \nciclos = {}".format(self.Ek,self.B,self.ang,self.ciclos))
        
        ax.set_xlabel('X [m]')
        ax.set_ylabel('Y [m]')
        ax.set_zlabel('Z [m]')

        ax.set_title('Trayectoria de particula cargada con v oblicua a B')
        ax.legend(loc = 'upper right')
        plt.savefig("xyz_particula.png")
        
        
        return

#debido que hasta este punto se observa una sola vuelta de la particula, se decide
#crear otra clase que herede todo de movParticula pero que permita observar varios ciclos

class movParticula2(movParticula):
    def __init__(self, tiempo, energia_particula, masa_particula, carga_particula, campo_magnetico, angulo, ciclos):
        super().__init__(tiempo, energia_particula, masa_particula, carga_particula, campo_magnetico, angulo)
        
        self.ciclos = ciclos

    def viewCycles(self):
        t = np.linspace(0,self.ciclos*super().periodo(),1000)
        return t