
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class TiroParabolico():
    #constructor
    def __init__(self,xi,h,v0,ang,t):

        #Atributos
        self.xi=xi
        self.h=h
        self.v0=v0
        self.ang=ang
        self.t=t
        self.g=-9.8 #aceleración gravitacional en la tierra
        self.v0x=v0*np.cos(ang*np.pi/180)
        self.v0y=v0*np.sin(ang*np.pi/180)

    def viewposx(self):
        px=self.xi + self.v0x*self.t
        return px

    def viewposy(self):
        py=self.h + self.v0y*self.t+0.5*self.g*self.t**2
        return py
    


class TiroParabolicoFcc(TiroParabolico):

    def __init__(self, xi, h, v0, ang,t,afr):
        super().__init__(xi, h, v0, ang, t)
        self.afr=afr

    def vfrx(self):
        px=self.xi + self.v0x*self.t-0.5*self.afr*self.t**2
        return px
        

    """
import math
import numpy as np

        #parametros
        self.velinit=velinit
        del.radinalpha=math.radians(alpha)
        self.g=g
        self.h0=h0
        self.x0=xo

    def velX(self):
        vel_x=self.velinit*round(math.cos(self.radinalpha),3)
        return vel_x

    def velY(self):
        vel_y=self.velinit*round(math.sin(self.radinalpha),3)
        return vel_y

    def tMaxVuelo(self):
        try:
            #tmax=-2*self.velY()/self.g
            tmax=(-self.velY()-np.sqrt(self.velY()**2-2*self.g*self.h0))/(self.g)
            return tmax
        except:
            return "Error en calculo de tmax, revisar parametros"  
    def arrTime(self):
        arr_time=np.arange(0,self.tMAxVuelo(),0.001)}
        return arr_time
    
    def posX(self):
        posx=[self.x0+i*self.velX() for i in self.arrTime()]    
        return posx

    def posY(self):
        posy=[self.h0+i*self.velY()+(1/2)*self.g*i**2 for i in self.arrTime()]    
        return posy

    def figMp(self):
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(),self.posY())
        plt.savefig("tiro_aprabolico.png")      
    """

        
