import numpy as np
import matplotlib.pyplot as plt

#Algoritmo para el modelo de Ising
#Este codigo se trabaja con una matriz de particulas 2x2
class Modelo_ISING():

    L=2 #matriz 2x2
    def __init__(self,npaso):

        self.N=self.L*self.L #Tamaño de la matriz de particulas
        self.npaso=npaso #Numero de pasos
        self.T=np.arange(1,7,0.01) #Arreglo de Temperatura en K


    def diccionario_matriz(self): #Metodo diccionario
        #Creamos un diccionario con los vecinos de cada particula 
        diccionario_vecinos={i : ((i // self.L)*self.L+(i+1)% self.L, (i+self.L)%self.N,(i // self.L)*self.L+(i-1)% self.L, (i-self.L)%self.N) for i in range(self.N)}
        return diccionario_vecinos
    
    def Montecarlo(self): #Metodo Montecarlo
        np.random.seed(0) #Semilla
        Espines=[np.random.choice([1,-1]) for k in range(self.N)] #Lista aleatoria de los espines de cada particula
        Cv_array = [] #Arreglo de los calores especificos para cada T

        for i in self.T:
            B=1/i #Parametro B=1/T

            Energia=0
            Energia_t=0
            Energia_t_2=0

            for paso in range(self.npaso):
                k=np.random.randint(0, self.N-1) #Numeros aleatorios enteros entre 0 a 2
                h=sum(Espines[nn] for nn in self.diccionario_matriz()[k]) #Sumatoria de los espines vecinos de cada particula
                Espines_old=Espines[k]
                Espines[k]=-1
                if np.random.uniform(0,1)< 1/(1+np.exp(-B*2*h)): #si corte < Probabilidad cambie de espin
                    Espines[k] =1

                if Espines[k]!=Espines_old:  
                    Energia-=2*Espines[k]*h #Hamiltoniano(energia) de la suma sobre todos los pares de vecinos cercanos

                Energia_t += Energia
                Energia_t_2 += Energia**2

                #Energia_prom=Energia_t/self.npaso
                #Energia_prom_2=Energia_t_2/self.npaso

            C_v=B**2/self.N*((Energia_t_2/self.npaso)-(Energia_t/self.npaso)**2)
            Cv_array.append(C_v)
        return Cv_array
    
    def grafica(self):
        plt.scatter(self.T,self.Montecarlo(),s=3)
        plt.ylabel("C_esp")
        plt.xlabel("T (K)")
        plt.title("Modelo de ISING")
        plt.axvline(x=2.269,color="red",ls="--",label="T_c")
        plt.legend()
        plt.savefig("Modelo de ISING")

