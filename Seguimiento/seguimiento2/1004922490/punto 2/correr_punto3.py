
import numpy as np
from punto_3 import Integral

if __name__ =='__main__':

  L=0.5#tamaño de la caja
  a=0
  b=L/4
  N=1000
  solution=Integral(a,b,N,L)
  
  

  print('El valor de la probabilidad con el método de Monte Carlo:', solution.valores_integral()[-1])
  print('El Valor exacto de la probabilidad utilizando scipy.integrate.quad:', solution.integral_real())






